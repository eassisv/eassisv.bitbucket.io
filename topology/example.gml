graph [
  Name "Network Name"
  Year "2000"
  node [
    id 1
    Name "Chapecó"
    Type "OXC"
    Latitude -27.104
    Longitude -52.614
  ]
  node [
    id 2
    Name "UFFS"
    Type "OXC"
    Latitude -27.114
    Longitude -52.707
  ]
  edge [
    source 1
    target 2
    Name "Chapecó/UFFS"
    Length 9.27
    ChannelCapacity 10
    FiberCapacity 10
  ]
]
